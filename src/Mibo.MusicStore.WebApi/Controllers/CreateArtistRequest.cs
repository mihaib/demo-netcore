﻿namespace Mibo.MusicStore.WebApi.Controllers
{
    public class CreateArtistRequest
    {
        public int Age { get; internal set; }
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
    }
}