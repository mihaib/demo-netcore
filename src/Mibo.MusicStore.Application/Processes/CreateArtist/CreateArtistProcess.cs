﻿using Mibo.MusicStore.Application.Processes.CreateArtist.Dto;
using Mibo.MusicStore.Application.Services;
using Mibo.MusicStore.Model;
using System;

namespace Mibo.MusicStore.Application.Processes.CreateAuthor
{
    public class CreateArtistProcess : ICreateArtistProcess
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateArtistProcess(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreateArtist(CreateArtistDto createArtistDto)
        {
            if (string.IsNullOrWhiteSpace(createArtistDto?.FirstName))
                throw new ArgumentException("FirstName is empty");

            if (string.IsNullOrWhiteSpace(createArtistDto?.LastName))
                throw new ArgumentException("LastName is empty");

            var dataModel = MapArtist(createArtistDto);
            _unitOfWork.GetRepository<Artist>().Insert(dataModel);
            _unitOfWork.Save();
        }

        private Artist MapArtist(CreateArtistDto createArtistDto)
        {
            return new Artist()
            {
                LastName = createArtistDto.LastName,
                FirstName = createArtistDto.FirstName,
                Age = createArtistDto.Age
            };
        }
    }
}
