﻿using Mibo.MusicStore.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mibo.MusicStore.Application.Services
{
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : class, IEntity;

        void Save();

        Task SaveAsync();
    }
}
