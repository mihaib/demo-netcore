﻿using Mibo.MusicStore.Domain;

namespace Mibo.MusicStore.Model
{
    public class Artist : IEntity
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public int Age { get; set; }
    }
}
