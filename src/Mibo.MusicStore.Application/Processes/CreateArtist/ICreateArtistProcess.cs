﻿using Mibo.MusicStore.Application.Processes.CreateArtist.Dto;

namespace Mibo.MusicStore.Application.Processes.CreateAuthor
{
    public interface ICreateArtistProcess
    {
        void CreateArtist(CreateArtistDto createArtistDto);
    }
}