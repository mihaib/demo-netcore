﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mibo.MusicStore.Application.Processes.CreateArtist.Dto
{
    public class CreateArtistDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }
    }
}
