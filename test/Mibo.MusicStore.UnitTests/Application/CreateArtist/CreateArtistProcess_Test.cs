﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using Mibo.MusicStore.Application.Processes.CreateArtist.Dto;
using Mibo.MusicStore.Application.Processes.CreateAuthor;
using Mibo.MusicStore.Application.Services;
using Mibo.MusicStore.Model;
using NSubstitute;
using NUnit.Framework;

namespace Mibo.MusicStore.UnitTests.Application.CreateArtist
{
    [TestFixture]
    public class CreateArtistProcess_Test
    {
        private IFixture _fixture;

        [SetUp]
        public void SetUp()
        {
            _fixture = new Fixture().Customize(new AutoNSubstituteCustomization());
        }


        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void When_creating_an_artist_with_empty_first_name_Then_exception_is_thrown(string name)
        {
            var dto = _fixture.Create<CreateArtistDto>();
            var sut = _fixture.Create<CreateArtistProcess>();
            dto.FirstName = name;

            Assert.That(() => sut.CreateArtist(dto), Throws.ArgumentException);
        }


        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void When_creating_an_artist_with_empty_last_name_Then_exception_is_thrown(string name)
        {
            var dto = _fixture.Create<CreateArtistDto>();
            var sut = _fixture.Create<CreateArtistProcess>();
            dto.LastName = name;

            Assert.That(() => sut.CreateArtist(dto), Throws.ArgumentException);
        }


        [Test]
        public void When_creating_an_artist_Then_dto_is_mapped_as_expected()
        {
            //arrange
            var repo = _fixture.Freeze<IRepository<Artist>>();
            var uof = _fixture.Freeze<IUnitOfWork>();
            uof.GetRepository<Artist>()
               .Returns(repo);

            var dto = _fixture.Create<CreateArtistDto>();

            Artist result = null;
            repo
                .When(r => r.Insert(Arg.Any<Artist>()))
                .Do(d => result = d.Arg<Artist>());

            //act
            var sut = _fixture.Create<CreateArtistProcess>();
            sut.CreateArtist(dto);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(result.FirstName, Is.EqualTo(dto.FirstName));
                Assert.That(result.LastName, Is.EqualTo(dto.LastName));
                Assert.That(result.Age, Is.EqualTo(dto.Age));
            });
        }
    }
}
