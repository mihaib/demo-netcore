﻿using System.Net;
using Mibo.MusicStore.Application.Processes.CreateArtist.Dto;
using Mibo.MusicStore.Application.Processes.CreateAuthor;
using Microsoft.AspNetCore.Mvc;

namespace Mibo.MusicStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtistsController : ControllerBase
    {
        private readonly ICreateArtistProcess _createArtistProcess;

        public ArtistsController(ICreateArtistProcess createArtistProcess)
        {
            _createArtistProcess = createArtistProcess;
        }


        [HttpPost("")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public ActionResult CreateArtist(CreateArtistRequest request)
        {
            _createArtistProcess.CreateArtist(CreateArtistDto(request));

            return Ok();
        }

        private static CreateArtistDto CreateArtistDto(CreateArtistRequest req)
        {
            return new CreateArtistDto
            {
                Age = req.Age,
                FirstName = req.FirstName,
                LastName = req.LastName
            };
        }
    }
}