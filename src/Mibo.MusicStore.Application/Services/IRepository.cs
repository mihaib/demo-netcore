﻿using Mibo.MusicStore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mibo.MusicStore.Application.Services
{
    public interface IRepository<T> where T : IEntity
    {
        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");

        T GetByID(object id);

        void Insert(T entity);

        void Update(T entity);

        void DeleteByID(object id);

        void Delete(T entityToDelete);
    }
}   
